{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DeriveAnyClass        #-}
{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE PolyKinds             #-}
{-# LANGUAGE RankNTypes            #-}
{-# LANGUAGE RecordWildCards       #-}
{-# LANGUAGE ScopedTypeVariables   #-}

module Bootstrap.Bounds.Spectrum where

import           Blocks          (Delta (..))
import           Data.Aeson      (FromJSON, ToJSON)
import           Data.Binary     (Binary)
import           Data.Map.Strict (Map)
import qualified Data.Map.Strict as Map
import           Data.Set        (Set)
import qualified Data.Set        as Set
import           GHC.Generics    (Generic)

data Spectrum r = Spectrum
  { twistGap       :: Rational
  , deltaGaps      :: Map r Delta
  , deltaIsolateds :: Map r (Set Delta)
  } deriving (Show, Eq, Ord, Generic, Binary, FromJSON, ToJSON)

-- | Specifies whether a value of dimension represents an isolated
-- operator or a continuum above some gap.
data DeltaRange = Isolated | Continuum
  deriving (Eq, Ord, Show)

unitarySpectrum :: Spectrum r
unitarySpectrum = Spectrum
  { twistGap = 0
  , deltaGaps = Map.empty
  , deltaIsolateds = Map.empty
  }

scalarsAbove :: Rational -> Spectrum Int
scalarsAbove delta = setGap 0 delta unitarySpectrum

defaultGap :: Spectrum r -> Delta
defaultGap spect = RelativeUnitarity (twistGap spect)

deltaGap :: Ord r => Spectrum r -> r -> Delta
deltaGap spect rep =
  Map.findWithDefault (defaultGap spect) rep (deltaGaps spect)

deltaIsolated :: Ord r => Spectrum r -> r -> Set Delta
deltaIsolated spect rep =
  Map.findWithDefault Set.empty rep (deltaIsolateds spect)

setGap :: Ord r => r -> Rational -> Spectrum r -> Spectrum r
setGap rep delta spect =
  spect { deltaGaps = Map.insert rep (Fixed delta) (deltaGaps spect) }

setGaps :: Ord r => [(r, Rational)] -> Spectrum r -> Spectrum r
setGaps gaps = foldr (.) id (map (uncurry setGap) gaps)

setTwistGap :: Rational -> Spectrum r -> Spectrum r
setTwistGap tau spect = spect { twistGap = tau }

addIsolated :: Ord r => r -> Rational -> Spectrum r -> Spectrum r
addIsolated rep delta spect =
  spect { deltaIsolateds = Map.alter addIso rep (deltaIsolateds spect) }
  where
    addIso Nothing       = Just (Set.singleton (Fixed delta))
    addIso (Just deltas) = Just (Set.insert (Fixed delta) deltas)

listDeltas :: Ord r => r -> Spectrum r -> [(Delta, DeltaRange)]
listDeltas r spect = continuum : isolated
  where
    isolated  = [(delta, Isolated) | delta <- Set.toList (deltaIsolated spect r)]
    continuum = (deltaGap spect r, Continuum)

