{-# LANGUAGE DataKinds          #-}
{-# LANGUAGE DefaultSignatures  #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE OverloadedStrings  #-}
{-# LANGUAGE PatternSynonyms    #-}

module Bootstrap.Bounds.Crossing.Util
  ( equalCrossingEqs
  , numCrossingEqs
  , writeCrossingEqs
  , Tuple4
  ) where

import Blocks                                      (Taylors)
import Bootstrap.Bounds.Crossing.CrossingEquations (FourPointFunctionTerm)
import Bootstrap.Bounds.ToTeX                      (ToTeX (..))
import Bootstrap.Math.FreeVect                     (FreeVect, vec)
import Bootstrap.Math.FreeVect                     qualified as FreeVect
import Bootstrap.Math.VectorSpace                  ((^/))
import Data.Foldable                               qualified as Foldable
import Data.Set                                    qualified as Set
import Data.Text                                   qualified as Text
import Data.Text.IO                                qualified as Text

type Tuple4 a = (a,a,a,a)

data FourPtFn o f = FourPtFn (Tuple4 o) f
  deriving (Eq, Ord, Show)

instance (ToTeX o, ToTeX f) => ToTeX (FourPtFn o f) where
  toTeX (FourPtFn (o1,o2,o3,o4) struct) =
    "g_{" <> Text.intercalate " " (map toTeX [o1,o2,o3,o4]) <> "}^{" <> toTeX struct <> "}"

dummyG :: (Num a, Eq a) => FourPointFunctionTerm o f (FourPtFn o f) a
dummyG o1 o2 o3 o4 q = vec (FourPtFn (o1,o2,o3,o4) q)

-------------------- Checking equality ---------------------

-- | TODO: Move to Bootstrap.Math.FreeVect?
normalizeFreeVect :: (Ord b, Fractional a, Eq a) => FreeVect b a -> FreeVect b a
normalizeFreeVect v = case FreeVect.toList v of
  []         -> v
  (_, c) : _ -> v ^/ c

-- WARNING: This function could fail due to floating point arithmetic
-- if the crossing equations have coefficients that are not exactly
-- representable by Double. It should be OK with only +- 1's, though.
equalCrossingEqs
  :: (Ord o, Ord f, Foldable v1, Foldable v2)
  => Int
  -> (FourPointFunctionTerm o f (FourPtFn o f) Double
      -> v1 (Taylors c, FreeVect (FourPtFn o f) Double))
  -> (FourPointFunctionTerm o f (FourPtFn o f) Double
      -> v2 (Taylors c, FreeVect (FourPtFn o f) Double))
  -> Bool
equalCrossingEqs lambda eqs1 eqs2 =
  Set.fromList (go (Foldable.toList (eqs1 dummyG))) ==
  Set.fromList (go (Foldable.toList (eqs2 dummyG)))
  where
    go = map (\(ts,v) -> (ts lambda, normalizeFreeVect v))

-- The following evaluates to 'True':
--
-- >>> equalCrossingEqs 11 (crossingEqsStructSet [((T,T,T,T), structsTTTT)])
--       (\g -> TTTT3d.crossingEqsTTTT3d (mapOps mapOpTInv g))
--
-- >>> equalCrossingEqs 11 crossingEqsIsingTSig' crossingEqsTSig3d
--
-- And similarly for various lambda. Thus, the "automatic" format
-- above works for reproducing the crossing equations in the TTTT
-- system and the T-Sig system.

numCrossingEqs
  :: Foldable v
  => (FourPointFunctionTerm o f (FourPtFn o f) Double
      -> v (Taylors c, FreeVect (FourPtFn o f) Double))
  -> Int
numCrossingEqs eqs = Foldable.length $ eqs dummyG

-------------------- Pretty printing ---------------------

data CrossingEq c o f a = CrossingEq (Taylors c) (FreeVect (FourPtFn o f) a)

instance (ToTeX o, ToTeX f, ToTeX a, Num a, Ord a) => ToTeX (CrossingEq c o f a) where
  toTeX (CrossingEq ts f) =
    "& \\partial_x^m \\partial_t^n \\left(" <> toTeX f <>
    "\\right), && {\\scriptstyle (m,n) \\in" <> toTeX ts <> "}"

data CrossingEqs v c o f a = CrossingEqs (v (Taylors c, FreeVect (FourPtFn o f) a))

instance (Foldable v, ToTeX o, ToTeX f, ToTeX a, Num a, Ord a) => ToTeX (CrossingEqs v c o f a) where
  toTeX (CrossingEqs eqs) = mconcat
    [ "\\begin{align*}"
    , Text.intercalate "\\\\ \n" $
      map (toTeX . (\(ts,gs) -> CrossingEq ts gs)) $
      Foldable.toList eqs
    , "\\end{align*}"
    ]

writeCrossingEqs
  :: (Foldable v, ToTeX o, ToTeX f)
  => FilePath
  -> (FourPointFunctionTerm o f (FourPtFn o f) Double ->
       v (Taylors c, FreeVect (FourPtFn o f) Double))
  -> IO ()
writeCrossingEqs path eqs = Text.writeFile path $
  toTeX $ CrossingEqs $ eqs dummyG
