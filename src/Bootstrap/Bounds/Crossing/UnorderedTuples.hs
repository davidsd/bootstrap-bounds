{-# LANGUAGE DataKinds           #-}
{-# LANGUAGE DeriveFunctor       #-}
{-# LANGUAGE MultiWayIf          #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeFamilies        #-}
{-# LANGUAGE TypeOperators       #-}

module Bootstrap.Bounds.Crossing.UnorderedTuples
  ( UPair
  , UTriple
  , uPair
  , uTriple
  , toTuple2
  , toTuple3
  ) where

newtype UPair a =
  UPair (a, a)
  deriving (Ord, Eq, Show, Functor)

newtype UTriple a =
  UTriple (a, a, a)
  deriving (Ord, Eq, Show, Functor)

uPair :: Ord a => a -> a -> UPair a
uPair a b
  | a <= b    = UPair (a, b)
  | otherwise = UPair (b, a)

uTriple :: Ord a => a -> a -> a -> UTriple a
uTriple a b c = UTriple $ if | c <= x    -> (c,x,y)
                             | c >= y    -> (x,y,c)
                             | otherwise -> (x,c,y)
  where
    x = min a b
    y = max a b

toTuple2 :: UPair a -> (a, a)
toTuple2 (UPair t) = t

toTuple3 :: UTriple a -> (a, a, a)
toTuple3 (UTriple t) = t

instance (Bounded a, Enum a, Eq a) => Enum (UPair a) where
  toEnum n =
    if n >= 0 && n <= upper
      then UPair (toZeroBased $ fst $ coords n, toZeroBased $ snd $ coords n)
      else error "toEnum{OperatorPair}: tag is outside of enumeration's range"
    where
      toZeroBased :: Int -> a
      toZeroBased m = toEnum $ m + fromEnum (minBound :: a)
      -- Can be replaced by a formula
      coords :: Int -> (Int, Int)
      coords 0 = (0, 0)
      coords m =
        let c = coords (m - 1)
         in if snd c < numa - 1
              then (fst c, snd c + 1)
              else (fst c + 1, fst c + 1)
      numa = fromEnum (maxBound :: a) - fromEnum (minBound :: a) + 1
      upper = (numa * (numa + 1)) `div` 2 - 1
  fromEnum (UPair (a, b)) = fromCoords (fromZeroBased a) (fromZeroBased b)
    where
      fromZeroBased :: a -> Int
      fromZeroBased x = fromEnum x - fromEnum (minBound :: a)
      fromCoords :: Int -> Int -> Int
      fromCoords m n = m * numa + n - (m * (m + 1)) `div` 2
      numa = fromEnum (maxBound :: a) - fromEnum (minBound :: a) + 1
  succ (UPair (a, b)) =
    if b /= maxBound
      then UPair (a, succ b)
      else UPair (succ a, succ a)
  pred (UPair (a, b)) =
    if b /= a
      then UPair (a, pred b)
      else UPair (pred a, maxBound)

instance Bounded a => Bounded (UPair a) where
  minBound = UPair (minBound, minBound)
  maxBound = UPair (maxBound, maxBound)
