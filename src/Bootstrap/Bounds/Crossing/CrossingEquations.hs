{-# LANGUAGE DataKinds                 #-}
{-# LANGUAGE ExistentialQuantification #-}
{-# LANGUAGE FunctionalDependencies    #-}
{-# LANGUAGE MultiWayIf                #-}
{-# LANGUAGE PolyKinds                 #-}
{-# LANGUAGE RankNTypes                #-}
{-# LANGUAGE ScopedTypeVariables       #-}
{-# LANGUAGE TypeApplications          #-}
{-# LANGUAGE TypeOperators             #-}

module Bootstrap.Bounds.Crossing.CrossingEquations where

import Blocks                                    (Block (..), CrossingMat)
import Bootstrap.Bounds.Crossing.UnorderedTuples (uTriple)
import Bootstrap.Math.FreeVect                   (FreeVect)
import Bootstrap.Math.FreeVect                   qualified as FV
import Bootstrap.Math.Linear                     qualified as L
import Bootstrap.Math.VectorSpace                (Tensor)
import Bootstrap.Math.VectorSpace                qualified as VS
import Data.Coerce                               (coerce)
import Data.Functor.Compose                      (Compose (..))
import Data.Matrix.Static                        (Matrix)
import GHC.TypeNats                              (KnownNat)
import Linear.V                                  (V (..))

class HasRep o r | o -> r where
  rep :: o -> r

-- | An OPE coefficient 'lambda' is specified by giving the
-- coefficient of 'lambda' inside all three-point correlation
-- functions. We can write this schematically as a function
--
-- \(o1, o2, o3) -> <o1 o2 o3> |_lambda
--
-- where <o1 o2 o3> is the correlation function of the given operators
-- and x|_lambda denotes the coefficient of lambda inside x. After
-- currying, this is a function of type
--
-- o1 -> o2 -> o3 -> FreeVect s a             (*)
--
-- where o1, o2, o3 are the types of the three operators, s is the
-- type of three-point structures, and a is a base field (numerical
-- type).
--
-- As an example, consider an OPE coefficient for a three-point
-- function of two non-identical scalars Phi1 and Phi2 and a third
-- operator O_J with spin j. We have
--
-- <Phi1(x_1) Phi2(x_2) O_J(x_3)> = lambda_{Phi1 Phi2 O_J} * V_{3,12}^J / ...
-- <Phi2(x_1) Phi1(x_2) O_J(x_3)> = lambda_{Phi1 Phi2 O_J} * (-1)^J * V_{3,12}^J / ...
--
-- where ... are standard products of powers of x_{ij}, and V_{3,12}
-- is the standard 3-point structure from spinning conformal
-- correlators. Note that both equations above are physically
-- equivalent, but the structures on the right-hand side depend on the
-- ordering that the operators appear in the correlator on the left
-- hand side.  The OPE coefficient lambda_{Phi1 Phi2 O_J} can be
-- represented schematically by the pseudocode
--
-- \o1 o2 j -> case (o1, o2) of
--   (Phi1, Phi2) -> V_{3,12}^j
--   (Phi2, Phi1) -> (-1)^j * V_{3,12}^j
--   _            -> 0
--
-- (For simplicity, here we assume O_J always appears third in the
-- correlator.)

-- | An OPE coefficient between two external operators with type 'o'
-- and a third (unspecified) operator. By partially applying the type
-- (*) above to the third operator, we obtain
type OPECoefficient o s a = o -> o -> FV.FreeVect s a

-- | An OPE coefficient between three operators with the type
-- 'o'. Specializing (*), we obtain
type OPECoefficientExternal o s a = o -> o -> o -> FV.FreeVect s a

-- | OPE coefficient for two non-identical external operators 'o1' and
-- 'o2' and a third (unspecified) operator. It is given by two linear
-- combinations of three-point structures 's1' and 's2' corresponding
-- to the two possible orderings of the operators. See the example of
-- lambda_{Phi1 Phi2 O_J} above.
opeCoeffGeneric
  :: (Eq o, Ord s, Fractional a, Eq a)
  => o
  -> o
  -> FV.FreeVect s a
  -> FV.FreeVect s a
  -> OPECoefficient o s a
opeCoeffGeneric o1 o2 s1 s2 o1' o2' =
  if | (o1',o2') == (o1,o2) -> s1
     | (o1',o2') == (o2,o1) -> s2
     | otherwise            -> VS.zero

-- | OPE coefficient for two identical external operators and a third
-- (unspecified) operator. It is given by a single linear combination
-- of three-point structures, since there is only one ordering of the
-- two operators.
opeCoeffIdentical
  :: (Eq o, Ord s, Fractional a, Eq a)
  => o
  -> FV.FreeVect s a
  -> OPECoefficient o s a
opeCoeffIdentical o1 s1 = opeCoeffGeneric o1 o1 s1 s1

class ThreePointStructure s i r ri where
  -- | Create a three-point structure of type 's' from representations
  -- of type 'r', 'r', and 'ri' ("i" for "internal"), together with a
  -- three-point label 'i'
  makeStructure :: r -> r -> ri -> i -> s

-- | A version of 'opeCoeffGeneric' that uses the
-- 'ThreePointStructure' and 'HasRep' typeclasses to build structures
-- containing the appropriate representations.
opeCoeffGeneric_
  :: (Eq o, Ord s, Fractional a, Eq a, ThreePointStructure s i r ri, HasRep o r)
  => o
  -> o
  -> ri
  -> FV.FreeVect i a
  -> FV.FreeVect i a
  -> OPECoefficient o s a
opeCoeffGeneric_ o1 o2 r i1 i2 = opeCoeffGeneric o1 o2 (FV.mapBasis go1 i1) (FV.mapBasis go2 i2)
  where
    go1 = makeStructure (rep o1) (rep o2) r
    go2 = makeStructure (rep o2) (rep o1) r

-- | A version of 'opeCoeffIdentical' that uses the
-- 'ThreePointStructure' and 'HasRep' typeclasses.
opeCoeffIdentical_
  :: (Eq o, Ord s, Fractional a, Eq a, ThreePointStructure s i r ri, HasRep o r)
  => o
  -> ri
  -> FV.FreeVect i a
  -> OPECoefficient o s a
opeCoeffIdentical_ o1 r i1 = opeCoeffGeneric_ o1 o1 r i1 i1

-- | An OPE coefficient for three external operators with a single
-- unique permutation-invariant three-point structure.
opeCoeffExternalSimple
  :: (Ord o, Ord s, Fractional a, Eq a, ThreePointStructure s i r r, HasRep o r)
  => o
  -> o
  -> o
  -> FV.FreeVect i a
  -> OPECoefficientExternal o s a
opeCoeffExternalSimple o1 o2 o3 i o1' o2' o3' = if uTriple o1 o2 o3 == uTriple o1' o2' o3'
    then
      FV.mapBasis (makeStructure (rep o1') (rep o2') (rep o3')) i
    else
      VS.zero

---------- Utility functions for OPECoefficient's ----------

-- | Map a function over the struct of an OPECoefficient
mapOPEStruct
  :: (Num a, Eq a, Ord s')
  => (s -> s')
  -> OPECoefficient o s a
  -> OPECoefficient o s' a
mapOPEStruct f ope o1 o2 = FV.mapBasis f (ope o1 o2)

-- | Map a function over the struct of an OPECoefficientExternal
mapOPEExtStruct
  :: (Num a, Eq a, Ord s')
  => (s -> s')
  -> OPECoefficientExternal o s a
  -> OPECoefficientExternal o s' a
mapOPEExtStruct f ope o1 o2 o3 = FV.mapBasis f (ope o1 o2 o3)

-- | Change operators of an OPECoefficient
mapOPEOps
  :: (Ord s, Num a, Eq a)
  => (o' -> Maybe o)
  -> OPECoefficient o s a
  -> OPECoefficient o' s a
mapOPEOps changeops ope o1 o2 =
  case (changeops o1, changeops o2) of
    (Just o1', Just o2') -> ope o1' o2'
    _                    -> 0

-- | Change the operators of an OPECoefficientExternal
mapOPEExtOps
  ::  (Ord s, Num a, Eq a)
  => (o' -> Maybe o)
  -> OPECoefficientExternal o s a
  -> OPECoefficientExternal o' s a
mapOPEExtOps changeops ope o1 o2 o3 = do
  case (changeops o1, changeops o2, changeops o3) of
    (Just o1', Just o2', Just o3') -> ope o1' o2' o3'
    _                              -> 0

type Fn = (->)

-- | An equivalent type to OPECoefficient o s, but with a VectorSpace instance
type OPECoefficientVS o s = (Fn o `Tensor` Fn o) `Tensor` FreeVect s

toOpeVS :: OPECoefficient o s a -> OPECoefficientVS o s a
toOpeVS = coerce

fromOpeVS :: OPECoefficientVS o s a -> OPECoefficient o s a
fromOpeVS = coerce

-- | An equivalent type to OPECoefficientExternal o s, but with a VectorSpace instance
type OPECoefficientExternalVS o s = ((Fn o `Tensor` Fn o) `Tensor` Fn o) `Tensor` FreeVect s

toOpeExtVS :: OPECoefficientExternal o s a -> OPECoefficientExternalVS o s a
toOpeExtVS = coerce

fromOpeExtVS :: OPECoefficientExternalVS o s a -> OPECoefficientExternal o s a
fromOpeExtVS = coerce

-- | Add two OPECoefficient's
addOPECoeff
  :: (Ord s, Num a, Eq a)
  => OPECoefficient o s a
  -> OPECoefficient o s a
  -> OPECoefficient o s a
addOPECoeff ope1 ope2 = fromOpeVS $ toOpeVS ope1 `VS.add` toOpeVS ope2

-- | Scale an OPECoefficient
scaleOPECoeff
  :: (Ord s, Num a, Eq a)
  => a
  -> OPECoefficient o s a
  -> OPECoefficient o s a
scaleOPECoeff x ope = fromOpeVS $ x VS.*^ toOpeVS ope

-- | Add two OPECoefficientExternal's
addOPECoeffExt
  :: (Ord s, Num a, Eq a)
  => OPECoefficientExternal o s a
  -> OPECoefficientExternal o s a
  -> OPECoefficientExternal o s a
addOPECoeffExt ope1 ope2 = fromOpeExtVS $ toOpeExtVS ope1 `VS.add` toOpeExtVS ope2

-- | Scale an OPECoefficientExternal
scaleOPECoeffExt
  :: (Ord s, Num a, Eq a)
  => a
  -> OPECoefficientExternal o s a
  -> OPECoefficientExternal o s a
scaleOPECoeffExt x ope = fromOpeExtVS $ x VS.*^ toOpeExtVS ope

-- | The OPE of two operators with the identity always has a unique
-- structure, determined by the representation of the two operators
-- (which must be identical). Thus, we can use the represntation as a
-- three-point structure label.
identityOpe :: (HasRep o r, Eq o, Ord r, Num a, Eq a) => OPECoefficient o r a
identityOpe o1 o2
  | o1 == o2  = FV.vec (rep o1)
  | otherwise = 0

---------- Four point functions ----------

type FourPointFunctionTerm o s b a = o -> o -> o -> o -> s -> FreeVect b a

mapOps :: (o -> o') -> FourPointFunctionTerm o' s b a -> FourPointFunctionTerm o s b a
mapOps f g o1 o2 o3 o4 s = g (f o1) (f o2) (f o3) (f o4) s

map4pt :: (s -> s') -> FourPointFunctionTerm o s' b a -> FourPointFunctionTerm o s b a
map4pt f g o1 o2 o3 o4 s = g o1 o2 o3 o4 (f s)

-- | A collection of crossing equations. Here, 'FourPointFunctionTerm
-- e s4 (Block t s4) a' is an abstract four-point function which we
-- can call 'g'. 'V n (derivs, FreeVect (Block t s4) a)' is a vector
-- of pairs of derivatives and linear combinations of blocks built
-- using 'g'.
type CrossingEquations n derivs e t s4 a =
  FourPointFunctionTerm e s4 (Block t s4) a -> V n (derivs, FreeVect (Block t s4) a)

-- | The contribution to the four-point function <o1 o2 o3 o4>
-- proportional to lambda_i lambda_j is given by
--
-- lambda_i o1 o2 >< lambda_j o4 o3
--
-- where we think of lambda_i as a function of type 'e -> e ->
-- FreeVect s a', and >< denotes merging two three-point structures
-- into a conformal block.
--
-- Note in particular the orderings 'o1 o2' and 'o4 o3' above.
--
-- Given crossing equations and a vector of OPE coefficients
-- (lambda_1, lambda_2, ...), we build a matrix of contributions
-- proportional to lambda_i lambda_j.
crossingMatrix :: (KnownNat j, Ord t, KnownNat n, Ord s4, Fractional a, Eq a)
  => V j (OPECoefficient e t a)
  -> CrossingEquations n derivs e t s4 a
  -> CrossingMat j n (Block t s4) a
crossingMatrix opeCoeffs crossingEqs =
  L.symmetrize $ Compose $ L.outerSquareWith go opeCoeffs
  where
    go ope1 ope2 = Compose . fmap snd . crossingEqs $ g ope1 ope2
    g ope1 ope2 o1 o2 o3 o4 s = FV.multiplyWith
      (\bl br -> Block bl br s)
      (ope1 o1 o2)
      (ope2 o4 o3)

-- | Given a vector of OPE coefficients for external operators,
-- crossing equations, and a list of external operators 'ops', build a
-- matrix of contributions of OPE coefficients involving the operators
-- in the crossing equations and 'ops'. Note again the orderings 'o1
-- o2' and 'o4 o3'.
crossingMatrixExternal :: (KnownNat j, Ord t, KnownNat n, Ord s4, Fractional a, Eq a)
  => V j (OPECoefficientExternal e t a)
  -> CrossingEquations n derivs e t s4 a
  -> [e]
  -> CrossingMat j n (Block t s4) a
crossingMatrixExternal opeCoeffs crossingEqs ops =
  L.symmetrize $ Compose $ L.outerSquareWith go opeCoeffs
  where
    go ope1 ope2 = Compose . fmap snd . crossingEqs $ g ope1 ope2
    g ope1 ope2 o1 o2 o3 o4 s = VS.sum $ do
        o <- ops
        pure $ FV.multiplyWith
          (\bl br -> Block bl br s)
          (ope1 o1 o2 o)
          (ope2 o4 o3 o)

derivsVec
  :: (FourPointFunctionTerm e s4 () Double -> V n (derivs, FreeVect () Double))
  -> V n derivs
derivsVec eqs = fst <$> eqs (\_ _ _ _ _ -> FV.vec ())

mapBlocks
  :: (Fractional a, Eq a, Ord b')
  => (b -> b')
  -> (Matrix j j `Tensor` V n `Tensor` FreeVect b) a
  -> (Matrix j j `Tensor` V n `Tensor` FreeVect b') a
mapBlocks f = VS.mapTensor (VS.mapTensor (FV.mapBasis f))

mapBlocksFreeVect
  :: (Fractional a, Eq a, Ord b')
  => (b -> FreeVect b' a)
  -> (Matrix j j `Tensor` V n `Tensor` FreeVect b) a
  -> (Matrix j j `Tensor` V n `Tensor` FreeVect b') a
mapBlocksFreeVect f = VS.mapTensor (VS.mapTensor (FV.evalFreeVect f))

identityCrossingMat
  :: (HasRep o r, Eq o, Ord r, Fractional a, Eq a, KnownNat n, Ord s4)
  => CrossingEquations n derivs o r s4 a
  -> CrossingMat 1 n (Block r s4) a
identityCrossingMat eqns =
  crossingMatrix (L.toV identityOpe) eqns
