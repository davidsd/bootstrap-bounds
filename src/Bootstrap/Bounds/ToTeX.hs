{-# LANGUAGE DataKinds          #-}
{-# LANGUAGE DefaultSignatures  #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE OverloadedStrings  #-}
{-# LANGUAGE PatternSynonyms    #-}

module Bootstrap.Bounds.ToTeX
  ( ToTeX(..)
  )where

import Blocks                     (Block (..), CrossingMat, Delta (..),
                                   Derivative (..), Sign (..), TaylorCoeff (..),
                                   Taylors)
import Bootstrap.Math.FreeVect    (FreeVect)
import Bootstrap.Math.FreeVect    qualified as FreeVect
import Bootstrap.Math.HalfInteger (HalfInteger)
import Data.Foldable              qualified as Foldable
import Data.Functor.Compose       (Compose (..))
import Data.Map.Strict            (Map)
import Data.Map.Strict            qualified as Map
import Data.Matrix.Static         (Matrix)
import Data.Matrix.Static         qualified as Matrix
import Data.Ratio                 (denominator, numerator)
import Data.Text                  (Text)
import Data.Text                  qualified as Text
import GHC.TypeNats               (KnownNat)
import Linear                     (transpose)

-- | A class for rendering 'a' to LaTeX code, represented as 'Text'.
class ToTeX a where
  toTeX :: a -> Text
  default toTeX :: Show a => a -> Text
  toTeX = Text.pack . show

instance ToTeX Double
instance ToTeX Int
instance ToTeX Integer

instance ToTeX Rational where
  toTeX r
    | r < 0              = "-" <> toTeX (-r)
    | denominator r == 1 = toTeX (numerator r)
    | otherwise          = "\\tfrac{" <> toTeX (numerator r) <> "}{" <> toTeX (denominator r) <> "}"

instance ToTeX HalfInteger where
  toTeX = toTeX . toRational

instance ToTeX (Sign k) where
  toTeX Plus  = "+"
  toTeX Minus = "-"

instance (ToTeX b, ToTeX a, Ord a, Num a) => ToTeX (FreeVect b a) where
  toTeX v = case FreeVect.toList v of
    [] -> "0"
    terms -> assemble $ do
      (t,c) <- terms
      let (sign, cString) = case c of
            1         -> (True, "")
            -1        -> (False, "")
            _ | c < 0 -> (False, toTeX (-c) <> " \\cdot ")
            _         -> (True, toTeX c <> " \\cdot ")
      pure (sign, cString <> toTeX t)
    where
      assemble ((True, s) : terms) = s <> mconcat (map withSign terms)
      assemble terms               = mconcat (map withSign terms)
      withSign (True, s)  = " + " <> s
      withSign (False, s) = " - " <> s

instance (ToTeX a1, ToTeX a2) => ToTeX (a1,a2) where
  toTeX (x1,x2) = "(" <> toTeX x1 <> "," <> toTeX x2 <> ")"

instance (ToTeX a1, ToTeX a2, ToTeX a3) => ToTeX (a1,a2,a3) where
  toTeX (x1,x2,x3) = "(" <> toTeX x1 <> "," <> toTeX x2 <> "," <> toTeX x3 <> ")"

instance ToTeX (Derivative c) where
  toTeX = toTeX . unDerivative

instance ToTeX d => ToTeX (TaylorCoeff d) where
  toTeX = toTeX . unTaylorCoeff

instance ToTeX (Taylors c) where
  toTeX taylors = "\\{ " <> Text.intercalate "," (map toTeX derivs) <> " \\}"
    where
      -- Should be big enough
      lambda = 5
      derivs = Foldable.toList (taylors lambda)

instance ToTeX Delta where
  toTeX (Fixed d)             = toTeX d
  toTeX (RelativeUnitarity 0) = "\\textrm{unitarity}"
  toTeX (RelativeUnitarity x) = "\\textrm{unitarity} + " <> toTeX x

instance (ToTeX a, ToTeX b) => ToTeX (Map a b) where
  toTeX m = Text.intercalate "\\\\ \n" $ do
    (x,y) <- Map.toList m
    pure $ toTeX x <> " &\\to& " <> toTeX y

instance (ToTeX t, ToTeX f) => ToTeX (Block t f) where
  toTeX (Block s12 s43 f) = mconcat
    [ "g_{\\begin{subarray}{l} "
    , toTeX s12
    , " \\\\ "
    , toTeX s43
    , "\\end{subarray}}^{"
    , toTeX f
    , "}"
    ]

instance (KnownNat n, ToTeX b, ToTeX a, Ord a, Num a) => ToTeX (CrossingMat j n b a) where
  toTeX m = mconcat $ do
    component <- Foldable.toList $ transpose $ fmap getCompose $ getCompose m
    pure $ mconcat $
      [ "\\begin{align*}\n"
      , toTeX component
      , "\n\\end{align*}\n"
      ]

instance ToTeX a => ToTeX (Matrix n m a) where
  toTeX m = mconcat
    [ "\\begin{pmatrix}"
    , Text.intercalate "\\\\\n" $ do
        row <- Matrix.toLists m
        pure $ Text.intercalate " & " (map toTeX row)
    , "\\end{pmatrix}"
    ]
