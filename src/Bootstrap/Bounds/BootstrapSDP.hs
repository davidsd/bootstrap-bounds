{-# LANGUAGE ApplicativeDo         #-}
{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DefaultSignatures     #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE GADTs                 #-}
{-# LANGUAGE InstanceSigs          #-}
{-# LANGUAGE MultiWayIf            #-}
{-# LANGUAGE OverloadedRecordDot   #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE TypeFamilies          #-}
{-# LANGUAGE TypeOperators         #-}

module Bootstrap.Bounds.BootstrapSDP where

import Blocks                                      (CrossingMat)
import Blocks                                      qualified as B
import Bootstrap.Bounds.Crossing.CrossingEquations (FourPointFunctionTerm,
                                                    derivsVec)
import Bootstrap.Bounds.Spectrum                   (DeltaRange (..))
import Bootstrap.Bounds.Util                       (runTagged)
import Bootstrap.Build.HasForce                    (HasForce)
import Bootstrap.Math.DampedRational               qualified as DR
import Bootstrap.Math.FreeVect                     (FreeVect)
import Bootstrap.Math.Linear                       (bilinearPair, fromM, fromV,
                                                    toV)
import Bootstrap.Math.VectorSpace                  (Tensor, mapTensor)
import Bootstrap.Math.VectorSpace                  qualified as VS
import Control.Monad.Zip                           (mzip)
import Data.Binary                                 (Binary)
import Data.Coerce                                 (coerce)
import Data.Distributive                           (distribute)
import Data.Functor.Compose                        (Compose (..))
import Data.Kind                                   (Type)
import Data.Matrix.Static                          qualified as M
import Data.Proxy                                  (Proxy (..))
import Data.Reflection                             (Reifies (..), reflect,
                                                    reify)
import Data.Tagged                                 (Tagged, proxy, untag)
import Data.Text                                   (Text)
import Data.Text                                   qualified as Text
import Data.Typeable                               (Typeable)
import Data.Vector                                 qualified as V
import Data.Vector.Instances                       ()
import GHC.Records                                 (HasField (..))
import GHC.TypeNats                                (KnownNat)
import Linear.V                                    (V, toVector)
import Linear.V                                    qualified as L
import SDPB                                        (MaybeMonadic (..),
                                                    Normalization (..),
                                                    NormalizationChunk (..),
                                                    Objective (..),
                                                    ObjectiveChunk (..),
                                                    PositiveConstraint (..),
                                                    PositiveConstraintChunk (..))
import SDPB qualified

data ConstraintConfig n deriv = MkConstraintConfig
  { numPoles :: Maybe Int
  , derivs   :: V n (V.Vector deriv)
  } deriving (Eq, Ord, Show)

crossingMatLabel
  :: (Binary a, Typeable a, Binary b, Typeable b, KnownNat j, KnownNat n)
  => Text
  -> CrossingMat j n b a
  -> SDPB.Label
crossingMatLabel name mat =
  SDPB.mkLabel name (fmap getCompose (getCompose mat))

-- | Break a CrossingMat corresponding to 'n' crossing equations into
-- a vector of CrossingMat's for individual equations.
breakCrossingMat :: KnownNat n => CrossingMat j n b a -> V n (CrossingMat j 1 b a)
breakCrossingMat =
  fmap (Compose . fmap Compose . fmap toV) .
  distribute .
  fmap getCompose .
  getCompose

-- | Break a CrossingMat and a corresponding collection of derivs
-- corresponding to 'n' equations into a vector of pairs of derivs and
-- CrossingMat's for individual equations.
breakDerivsCrossingMat
  :: KnownNat n
  => V n derivs
  -> CrossingMat j n b a
  -> V.Vector (V 1 derivs, CrossingMat j 1 b a)
breakDerivsCrossingMat dvs vs =
  toVector $ mzip (fmap toV dvs) (breakCrossingMat vs)

bootstrapObjective
  :: ( KnownNat n
     , Binary a
     , Typeable a
     , Binary b
     , Ord b
     , Typeable b
     , Applicative f
     , HasForce f
     , B.IsolatedBlock b deriv a
     , B.BlockFetchContext b a f
     )
  => B.BlockTableParams b
  -> V n (V.Vector deriv)
  -> CrossingMat 1 n b a
  -> SDPB.Objective f a
bootstrapObjective blockTableParams dvs vs =
  MkObjective
  { chunks = do
      (dv, v) <- breakDerivsCrossingMat dvs vs
      pure $ MkObjectiveChunk
        { vector =
            if v == VS.zero
            then
              Pure $ fmap (const 0) (fromV dv)
            else
              Monadic $
              fmap fromM $
              runTagged blockTableParams $
              getCompose $
              B.getCrossingMatIsolated dv v
        , label = crossingMatLabel "" v
        }
  , label = crossingMatLabel "" vs
  }

bootstrapNormalization
  :: ( KnownNat n
     , Binary a
     , Typeable a
     , Binary b
     , Ord b
     , Typeable b
     , Applicative f
     , HasForce f
     , B.IsolatedBlock b deriv a
     , B.BlockFetchContext b a f
     )
  => B.BlockTableParams b
  -> V n (V.Vector deriv)
  -> CrossingMat 1 n b a
  -> SDPB.Normalization f a
bootstrapNormalization blockTableParams dvs vs =
  MkNormalization
  { chunks = do
      (dv, v) <- breakDerivsCrossingMat dvs vs
      pure $ MkNormalizationChunk
        { vector =
            if v == VS.zero
            then
              Pure $ fmap (const 0) (fromV dv)
            else
              Monadic $
              fmap fromM $
              runTagged blockTableParams $
              getCompose $
              B.getCrossingMatIsolated dv v
        , label = crossingMatLabel "" v
        }
  , label = crossingMatLabel "" vs
  }

isolatedConstraint
  :: ( KnownNat n
     , KnownNat j
     , Binary a
     , Typeable a
     , Binary b
     , Ord b
     , Typeable b
     , Applicative f
     , HasForce f
     , B.IsolatedBlock b deriv a
     , B.BlockFetchContext b a f
     )
  => B.BlockTableParams b
  -> V n (V.Vector deriv)
  -> CrossingMat j n b a
  -> SDPB.PositiveConstraint f a
isolatedConstraint params dv mat =
  isolatedConstraint_ (constraintConfigFromDerivs dv) params "" mat

isolatedConstraint_
  :: ( KnownNat n
     , KnownNat j
     , Binary a
     , Typeable a
     , Binary b
     , Ord b
     , Typeable b
     , Applicative f
     , HasForce f
     , B.IsolatedBlock b deriv a
     , B.BlockFetchContext b a f
     )
  => ConstraintConfig n deriv
  -> B.BlockTableParams b
  -> Text
  -> CrossingMat j n b a
  -> SDPB.PositiveConstraint f a
isolatedConstraint_ cfg blockTableParams lbl vs =
  MkPositiveConstraint
  { chunks = do
      (dv, v) <- breakDerivsCrossingMat cfg.derivs vs
      pure $ MkPositiveConstraintChunk
        { matrices =
            if v == VS.zero
            then
              let
                zeroVector = fmap (const 0) (fromV dv)
                zeroMatrix = fmap (const zeroVector) (getCompose v)
              in
                Pure $ V.singleton $ SDPB.constantMatrix zeroMatrix
            else
              Monadic $
              fmap V.singleton $
              SDPB.constantMatrix <$>
              getIsolatedMat blockTableParams dv v
        , label = crossingMatLabel name v
        }
  , label = crossingMatLabel name vs
  }
  where
    name = concatNonEmpty ["isolated", lbl]

concatNonEmpty :: [Text] -> Text
concatNonEmpty = Text.intercalate "_" . filter (/= "")

getIsolatedMat
  :: (KnownNat n, Applicative f, HasForce f, B.IsolatedBlock b deriv a, B.BlockFetchContext b a f)
  => B.BlockTableParams b
  -> V n (V.Vector deriv)
  -> CrossingMat j n b a
  -> f (M.Matrix j j (V.Vector a))
getIsolatedMat blockTableParams dv v =
  runTagged blockTableParams $
  getCompose $
  B.getCrossingMatIsolated dv v

-- | The blocks libraries scalar_blocks and blocks_3d compute
-- conformal blocks as DampedRational's with exponential term
-- (4*rho)^Delta. However, it is expected that normalizing the
-- functional so that it behaves like rho^Delta at infinity leads to
-- better behavior, see (in progress). Thus, we divide the base by 4.
--
-- WARNING: We do this for *all* bases, even though it is only
-- necessarily a good idea for 4*rho -> rho. In the future, we could
-- make this configurable via ConstraintConfig.
--
data DivideBy4 base a

instance (Fractional a, Reifies base a) => Reifies (DivideBy4 base a) a where
  reflect :: proxy (DivideBy4 base a) -> a
  reflect _ = reflect @base Proxy / 4

-- | Divide the base in a 'DampedRational' by 4. Because
-- 'DampedRational's with different bases have the same runtime
-- representation, we can use 'coerce'.
divideBaseBy4
  :: forall (base :: Type) f a . DR.DampedRational base f a
  -> DR.DampedRational (DivideBy4 base a) f a
divideBaseBy4 = coerce

constraintConfigFromDerivs :: V n (V.Vector deriv) -> ConstraintConfig n deriv
constraintConfigFromDerivs dv = MkConstraintConfig Nothing dv

-- | Note: When forming a continnumConstraint, we cancel poles and
-- zeros between the numerator and denominator at the location of the
-- rightmost pole of the 'DampedRational'. Note that we expect the
-- rightmost pole to be protected, so its residue should not be
-- affected by pole shifting in conformal blocks code. Consequently,
-- if we use e.g. conserved three-point structures, we expect the
-- resulting zero and pole to be correctly cancelled here.
--
-- Note that DR.cancelRightmostPoleZeros determines whether a zero
-- exists by comparing to (sqrt epsilon), where epsilon is the
-- difference between 1 and the smallest RealFloat greater than
-- 1. Thus, if the arithmetic is sufficiently complicated that things
-- do not cancel within sqrt epsilon, the cancellation will not
-- occur. If this is happening, then the result will not be very
-- accurate anyway.
continuumConstraint_
  :: forall n j a b f deriv . ( KnownNat n, KnownNat j, Binary a, Typeable a, Binary b, Ord b, Typeable b, Applicative f, HasForce f
     , B.ContinuumBlock b deriv a
     , Reifies (B.BlockBase b a) a
     , B.BlockFetchContext b a f
     )
  => ConstraintConfig n deriv
  -> B.BlockTableParams b
  -> Text
  -> CrossingMat j n b a
  -> SDPB.PositiveConstraint f a
continuumConstraint_ cfg blockTableParams lbl vs =
  MkPositiveConstraint
  { chunks = do
      (dv, v) <- breakDerivsCrossingMat cfg.derivs vs
      pure $ MkPositiveConstraintChunk
        { matrices =
            if v == VS.zero
            then
              Pure $ V.singleton $ mkContinuumPositiveMatrix $ mkZeroContinuumMat dv v
            else
              Monadic $ do
              mat' <- getContinuumMat blockTableParams dv v
              pure $ V.singleton $ mkContinuumPositiveMatrix mat'
        , label = crossingMatLabel name v
        }
  , label = crossingMatLabel name vs
  }
  where
    name = concatNonEmpty ["continuum", lbl]
    mkContinuumPositiveMatrix mat' =
      let
        -- Divide base by 4 -- see discussion above
        mat = divideBaseBy4 . DR.mapNumerator (Compose . M.unpackStatic . getCompose) $ mat'
      in
        SDPB.mkPositiveMatrix mat cfg.numPoles

-- | Produce the zero matrix with the appropriate type returned by
-- getContinuumMat
mkZeroContinuumMat
  :: forall b deriv a j . (B.ContinuumBlock b deriv a, KnownNat j)
  => V 1 (V.Vector deriv)
  -> CrossingMat j 1 b a
  -> DR.DampedRational (B.BlockBase b a) (M.Matrix j j `Compose` V.Vector) a
mkZeroContinuumMat dv _ = L.reifyVectorNat (fromV dv) $ \(_ :: V l deriv) ->
  DR.mapNumerator (VS.mapTensor L.toVector) $
  VS.zero @(DR.DampedRational (B.BlockBase b a) (M.Matrix j j `Compose` V l))

continuumConstraint
  :: ( KnownNat n, KnownNat j, Binary a, Typeable a, Binary b, Ord b, Typeable b, Applicative f, HasForce f
     , B.ContinuumBlock b deriv a
     , Reifies (B.BlockBase b a) a
     , B.BlockFetchContext b a f
     )
  => B.BlockTableParams b
  -> V n (V.Vector deriv)
  -> CrossingMat j n b a
  -> SDPB.PositiveConstraint f a
continuumConstraint params dv mat =
  continuumConstraint_ (constraintConfigFromDerivs dv) params "" mat

getContinuumMat
  :: (KnownNat n, Applicative f, HasForce f, B.ContinuumBlock b deriv a, B.BlockFetchContext b a f)
  => B.BlockTableParams b
  -> V n (V.Vector deriv)
  -> CrossingMat j n b a
  -> f (DR.DampedRational (B.BlockBase b a) (M.Matrix j j `Tensor` V.Vector) a)
getContinuumMat blockTableParams dv v =
  runTagged blockTableParams $ getCompose $
  fmap DR.cancelRightmostPoleZeros $
  B.getCrossingMatContinuum dv v

bootstrapConstraint_
  :: forall b n j a f deriv.
     ( KnownNat n, KnownNat j, Binary a, Typeable a, Binary b, Ord b, Typeable b, Applicative f, HasForce f
     , B.ContinuumBlock b deriv a
     , Reifies (B.BlockBase b a) a
     , B.BlockFetchContext b a f
     )
  => ConstraintConfig n deriv
  -> B.BlockTableParams b
  -> DeltaRange
  -> Text
  -> CrossingMat j n b a
  -> SDPB.PositiveConstraint f a
bootstrapConstraint_ cfg blockTableParams r label v = case r of
  Isolated  -> isolatedConstraint_  cfg blockTableParams label v
  Continuum -> continuumConstraint_ cfg blockTableParams label v

bootstrapConstraint
  :: forall b n j a f deriv.
     ( KnownNat n, KnownNat j, Binary a, Typeable a, Binary b, Ord b, Typeable b, Applicative f, HasForce f
     , B.ContinuumBlock b deriv a
     , Reifies (B.BlockBase b a) a
     , B.BlockFetchContext b a f
     )
  => B.BlockTableParams b
  -> V n (V.Vector deriv)
  -> DeltaRange
  -> CrossingMat j n b a
  -> SDPB.PositiveConstraint f a
bootstrapConstraint params dv range mat =
  bootstrapConstraint_ (constraintConfigFromDerivs dv) params range "" mat

--------- "Classy" interface -----------

class HasBlockParams p s where
  getBlockParams :: s -> p

  default getBlockParams :: HasField "blockParams" s p => s -> p
  getBlockParams = getField @"blockParams"

instance HasBlockParams p p where
  getBlockParams = id

instance HasBlockParams () s where
  getBlockParams = const ()

instance (HasBlockParams p1 s, HasBlockParams p2 s) => HasBlockParams (p1, p2) s where
  getBlockParams s = (getBlockParams s, getBlockParams s)

class HasConstraintConfig n s where
  type Deriv s
  getConstraintConfig :: s -> ConstraintConfig n (Deriv s)

instance HasConstraintConfig n (ConstraintConfig n deriv) where
  type Deriv (ConstraintConfig n deriv) = deriv
  getConstraintConfig = id

defaultConstraintConfig
  :: forall s p e s4 n deriv .
     ( HasBlockParams p s
     , HasField "nmax" p Int
     , HasField "numPoles" s (Maybe Int)
     , KnownNat n
     )
  => Proxy p
  -> (FourPointFunctionTerm e s4 () Double -> V n (Int -> V.Vector deriv, FreeVect () Double))
  -> s
  -> ConstraintConfig n deriv
defaultConstraintConfig _ crossingEqs key =
  MkConstraintConfig key.numPoles $
    derivsVec crossingEqs <*> pure (getBlockParams @p key).nmax

objective
  :: forall k n b a f key .
     ( KnownNat n
     , Binary a
     , Typeable a
     , Binary b
     , Ord b
     , Typeable b
     , Applicative f
     , HasForce f
     , B.IsolatedBlock b (Deriv key) a
     , B.BlockFetchContext b a f
     , HasBlockParams (B.BlockTableParams b) key
     , HasConstraintConfig n key
     , Reifies k key
     )
  => (Tagged k `Tensor` CrossingMat 1 n b) a
  -> Tagged k (SDPB.Objective f a)
objective v =
  bootstrapObjective (getBlockParams s) (getConstraintConfig s).derivs <$> getCompose v
  where
    s = reflect @k Proxy

normalization
  :: forall k n b a f key .
     ( KnownNat n
     , Binary a
     , Typeable a
     , Binary b
     , Ord b
     , Typeable b
     , Applicative f
     , HasForce f
     , B.IsolatedBlock b (Deriv key) a
     , B.BlockFetchContext b a f
     , HasBlockParams (B.BlockTableParams b) key
     , HasConstraintConfig n key
     , Reifies k key
     )
  => (Tagged k `Tensor` CrossingMat 1 n b) a
  -> Tagged k (SDPB.Normalization f a)
normalization v =
  bootstrapNormalization (getBlockParams s) (getConstraintConfig s).derivs <$> getCompose v
  where
    s = reflect @k Proxy

constraint
  :: forall b n j a f k key .
     ( KnownNat n
     , KnownNat j
     , Binary a
     , Typeable a
     , Binary b
     , Ord b
     , Typeable b
     , Applicative f
     , HasForce f
     , B.ContinuumBlock b (Deriv key) a
     , Reifies (B.BlockBase b a) a
     , B.BlockFetchContext b a f
     , HasBlockParams (B.BlockTableParams b) key
     , HasConstraintConfig n key
     , Reifies k key
     )
  => DeltaRange
  -> Text
  -> (Tagged k `Tensor` CrossingMat j n b) a
  -> Tagged k (SDPB.PositiveConstraint f a)
constraint r label v =
  bootstrapConstraint_ (getConstraintConfig s) (getBlockParams s) r label <$> getCompose v
  where
    s = reflect @k Proxy

constConstraint
  :: forall b n j a f k key .
     ( KnownNat n
     , KnownNat j
     , Binary a
     , Typeable a
     , Binary b
     , Ord b
     , Typeable b
     , Applicative f
     , HasForce f
     , B.IsolatedBlock b (Deriv key) a
     , B.BlockFetchContext b a f
     , HasBlockParams (B.BlockTableParams b) key
     , HasConstraintConfig n key
     , Reifies k key
     )
  => Text
  -> (Tagged k `Tensor` CrossingMat j n b) a
  -> Tagged k (SDPB.PositiveConstraint f a)
constConstraint label v =
  isolatedConstraint_ (getConstraintConfig s) (getBlockParams s) label <$> getCompose v
  where
    s = reflect @k Proxy

sdp
  :: forall k n1 b1 n2 b2 a m key .
     ( KnownNat n1
     , KnownNat n2
     , Applicative m
     , HasForce m
     , Typeable a
     , Typeable b1
     , Typeable b2
     , Binary b1
     , Binary b2
     , Ord b1
     , Ord b2
     , Binary a
     , B.IsolatedBlock b1 (Deriv key) a
     , B.IsolatedBlock b2 (Deriv key) a
     , B.BlockFetchContext b1 a m
     , B.BlockFetchContext b2 a m
     , HasBlockParams (B.BlockTableParams b1) key
     , HasBlockParams (B.BlockTableParams b2) key
     , HasConstraintConfig n1 key
     , HasConstraintConfig n2 key
     , Reifies (k :: Type) key
     )
  => (Tagged k `Tensor` CrossingMat 1 n1 b1) a
  -> (Tagged k `Tensor` CrossingMat 1 n2 b2) a
  -> [Tagged k (SDPB.PositiveConstraint m a)]
  -> Tagged k (SDPB.SDP m a)
sdp obj norm cons = pure $ SDPB.SDP
  (untag @k $ objective obj)
  (untag @k $ normalization norm)
  (map (untag @k) cons)

pair
  :: (KnownNat j, KnownNat n, Ord b, Real a', Fractional a, Eq a)
  => V j a'
  -> (Tagged k `Tensor` CrossingMat j n b) a
  -> (Tagged k `Tensor` CrossingMat 1 n b) a
pair v = mapTensor (bilinearPair (fmap realToFrac v))

tagVec :: forall k f a . f a -> (Tagged k `Tensor` f) a
tagVec = Compose . pure

untagVec :: forall k f a . (Tagged k `Tensor` f) a -> f a
untagVec = untag . getCompose

mapTag
  :: forall (k :: Type) key key' r . Reifies k key
  => (key -> key')
  -> (forall (k' :: Type) . Reifies k' key' => Tagged k' r)
  -> Tagged k r
mapTag f x = pure $ reify (f (reflect @k Proxy)) (proxy x)

