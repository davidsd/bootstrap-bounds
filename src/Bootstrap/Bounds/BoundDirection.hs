{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric  #-}

module Bootstrap.Bounds.BoundDirection where

import           Data.Aeson   (FromJSON, ToJSON)
import           Data.Binary  (Binary)
import           GHC.Generics (Generic)

data BoundDirection
  = UpperBound
  | LowerBound
  deriving (Show, Eq, Ord, Enum, Bounded, Generic, Binary, ToJSON, FromJSON)

boundDirSign :: Num a => BoundDirection -> a
boundDirSign UpperBound = 1
boundDirSign LowerBound = -1
