{-# LANGUAGE DataKinds                 #-}
{-# LANGUAGE ExistentialQuantification #-}
{-# LANGUAGE FunctionalDependencies    #-}
{-# LANGUAGE MultiWayIf                #-}
{-# LANGUAGE PolyKinds                 #-}
{-# LANGUAGE RankNTypes                #-}
{-# LANGUAGE ScopedTypeVariables       #-}
{-# LANGUAGE TypeApplications          #-}
{-# LANGUAGE TypeOperators             #-}

module Bootstrap.Bounds.Util where

import Data.Kind       (Type)
import Data.Proxy      (Proxy)
import Data.Reflection (Reifies, reify)
import Data.Tagged     (Tagged, proxy, untag)

runTagged :: forall a r. a -> (forall (s :: Type). Reifies s a => Tagged s r) -> r
runTagged a f = reify a (proxy f)

runTagged2
  :: forall a b r.
     (a,b)
  -> (forall (s :: Type) (t :: Type) . (Reifies s a, Reifies t b) => Tagged '(s,t) r)
  -> r
runTagged2 (a,b) f =
 reify a $ \(_ :: Proxy s) ->
 reify b $ \(_ :: Proxy t) ->
 untag @'(s,t) f
