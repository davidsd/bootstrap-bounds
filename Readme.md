bootstrap-bounds
---------------

Haskell library setting up conformal crossing equations.

Documentation
-------------

[Documentation is here](https://davidsd.gitlab.io/bootstrap-bounds/)
